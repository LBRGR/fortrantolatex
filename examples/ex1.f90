program ex1
  use f2tex
  implicit none
  real(8), dimension(8,10) :: A 
  real(8), dimension(8,10) :: X 
  integer, dimension(9) :: lines
  integer :: fileunit
  integer :: i,j

  A = 1.0D0  
  do i=1,8
    do j=1,10
      A(i,j) = dble(i*i+j)
      X(i,j) = dble(i)
    enddo
  enddo

  
  lines = 0
  lines(1) = 1
  lines(9) = 1
  call writeTable(A,8,10)
  open(unit = fileunit,file="test.txt",form="formatted",action="write",access="sequential",status="replace")
  call writeTable(A,8,10,caption="TEST",fileunit=fileunit,fmt_cell="1f8.2",columns="|l|l|ccccc|ccc|",hline=lines)
  call writeGraph(X,A,8,2,axis="axis",legend="Case1,Case2",fileunit=fileunit,&
                 axis_options="xlabel=x,ylabel=y",&
                 plot_options= (/"color=red ",&
                                 "color=blue"/) )
  close(fileunit)

end program ex1
