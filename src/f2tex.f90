!   f2tex is a module compounded of two simple subroutines to write table
!   and 2D plots directely in LaTex
!   Copyright Laurent Berenguer

!   This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.

!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.

!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
MODULE f2tex 
  implicit none
  contains

  ! Subroutine to print a table
  ! Option list: A: n x m matrix
  ! caption: placed above
  ! fileunit: file to write
  ! columns: columns format
  ! fmt_cell: format of cells
  ! hline: array of size n+1. if hline(i) = 0 then the ith hline is not printed
  subroutine writeTable(A,n,m,caption,fileunit,columns,fmt_cell,hline)
    implicit none
    real(8), dimension(:,:), intent(in) :: A
    character(len=*), intent(in), optional  :: caption 
    character(len=5), intent(in), optional  :: fmt_cell 
    integer, intent(in) :: n,m
    integer, intent(in), optional :: fileunit
    integer, dimension(:), intent(in), optional :: hline
    character(len=*), intent(in), optional :: columns
    character(len=5) :: local_fmt="1f8.3"
    integer :: i,j, localfileunit

    localfileunit = 6
    if(present(fileunit)) then
      localfileunit = fileunit
    endif
    if(present(fmt_cell)) then
      local_fmt = fmt_cell 
    endif

    write(localfileunit,*) "\begin{table}" 


    if(present(caption)) then
      write(localfileunit,*) "\caption{",caption,"}"  
    endif

    if(present(columns)) then
      write(localfileunit,*) "\begin{tabular}{",columns,"}" 
    else
      write(localfileunit,"(16A)",advance="no") "\begin{tabular}{" 
      do j = 1,m   
        write(localfileunit,"(A)", advance="no") "c" 
      end do
      write(localfileunit,"(A)") "}" 
    endif

    if(present(hline)) then
      if(hline(1) .eq. 1) then
         write(localfileunit,"(x9A)") "\hline"  
       endif
    else 
      write(localfileunit,"(x9A)") "\hline"  
    endif

    do i = 1,n
      write(localfileunit,"(" // local_fmt //")", advance="no") A(i,1) 
      do j = 2,m   
        write(localfileunit,"(x,A,x,"// local_fmt //")", advance="no") "&",A(i,j) 
      end do
      if(present(hline)) then
        if(hline(i+1) .eq. 1) then
           write(localfileunit,"(x9A)") "\\ \hline"  
        else
            write(localfileunit,"(x9A)") "\\"  
        endif
      else 
        write(localfileunit,"(x9A)") "\\ \hline"  
      endif
    end do 

    write(localfileunit,*) "\end{tabular}" 
    write(localfileunit,*) "\end{table}" 

  end subroutine writeTable


 ! WARNING : for plot_options, all options must have the same length (add
 ! whitespace)
  subroutine writeGraph(X,Y,n,m,axis,axis_options,plot_options,legend,fileunit,fmt_cell)
    implicit none
    real(8), dimension(:,:), intent(in) :: X,Y
    character(len=*), intent(in) :: axis 
    character(len=*), intent(in),optional :: legend 
    character(len=*), intent(in),optional :: axis_options 
    character(len=5), intent(in), optional  :: fmt_cell 
    character(len=*), dimension(:), intent(in), optional  :: plot_options 
    integer, intent(in) :: n,m
    integer, intent(in), optional :: fileunit
    character(len=5) :: local_fmt="1f8.3"
    integer :: i,j, localfileunit

    localfileunit = 6
    if(present(fileunit)) then
      localfileunit = fileunit
    endif
    if(present(fmt_cell)) then
      local_fmt = fmt_cell 
    endif

    write(localfileunit,*) "\begin{tikzpicture}" 
    
    if(present(axis_options)) then
      write(localfileunit,"(A,A,A,A,A)") "\begin{",axis,"}[",axis_options,"]" 
    else
      write(localfileunit,*) "\begin{",axis,"}" 
    endif 

    do j=1,m
      if(present(plot_options)) then
         write(localfileunit,*) "\addplot[",TRIM(plot_options(j)),"] coordinates{"
      else
         write(localfileunit,*) "\addplot coordinates{"
      endif 

      do i=1,n
        write(localfileunit,"(A,"// local_fmt // ",A," // local_fmt //",A)") "(", X(i,j),",",Y(i,j),")"
      enddo
      write(localfileunit,*) "};"
    enddo
    if(present(legend)) then
      write(localfileunit,*) "\legend{",legend,"}" 
    endif
    write(localfileunit,*) "\end{",axis,"}" 
    write(localfileunit,*) "\end{tikzpicture}" 


  end subroutine writeGraph

END MODULE f2tex
